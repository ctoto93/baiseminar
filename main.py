import json
import sys
from genetic_algorithm import GeneticAlgorithm

COMMAND_NEW = "new"
COMMAND_TRAIN = "train"
COMMAND_SHOW = "show"

cmd = sys.argv[1]
if cmd == COMMAND_SHOW:
    fname = sys.argv[2]
    ga = GeneticAlgorithm.from_file(fname)
    while True:
        c = ga.population[0]
        c.evaluate(True)
        print("REWARD: ", c.fitness)
    # for i, chromesome in enumerate(ga.population):
    #     print('CHROMOSOME %d:' % i)
    #     chromesome.evaluate(render=True)
    #     print("REWARD: ", chromesome.fitness)


nep = int(sys.argv[2])
if cmd == COMMAND_NEW:
    ga = GeneticAlgorithm()
    ga.run(nep)
elif cmd == COMMAND_TRAIN:
    ga = GeneticAlgorithm.from_file()
    ga.run(nep)
