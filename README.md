# Installation
1. Install gym
    * git clone https://github.com/openai/gym.git
    * cd gym
    * pip install -e .
1. Install box2d-py
    * pip install box2d-py

# Command
1. main.py show {filename}
1. main.py new {generation (int)}
1. main.py train {generation (int)}

# Experiment data
1. relu_exploration.json => exploration
1. relu_exploitation.json => exploitation
1. balanced.json => control group
1. reconfigured.json => last tweaked hyperparameters

# Notes
1. hyperparameters can be set on constants.py
1. plots can be seen on data.ipynb
