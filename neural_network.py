import numpy as np
import math
from constants import *


class NeuralNetwork:
    def __init__(self, chromosome):
        self._chromosome = chromosome

    @staticmethod
    def sigmoid(x):
        shape = x.shape
        x = x.flatten()
        result = np.array(list(map(lambda val: 1 / (1 + math.exp(-val)), x)))
        return result.reshape(shape)

    @staticmethod
    def relu(x):
        shape = x.shape
        x = x.flatten()
        result = np.array(list(map(lambda val: max(0, val), x)))
        return result.reshape(shape)

    def predict(self, observation):
        input = observation.reshape(1, SIZE_OF_INPUT_NODES)
        weights_ih1 = self._chromosome.weights_ih1.reshape(SIZE_OF_INPUT_NODES, SIZE_OF_HIDDEN_LAYER_1)

        bias_h1 = self._chromosome.bias_h1.reshape(1, SIZE_OF_HIDDEN_LAYER_1)
        output_h1 = np.dot(input, weights_ih1) + bias_h1
        output_h1 = NeuralNetwork.relu(output_h1)

        weights_h1h2 = self._chromosome.weights_h1h2.reshape(SIZE_OF_HIDDEN_LAYER_1, SIZE_OF_HIDDEN_LAYER_2)
        bias_h2 = self._chromosome.bias_h2.reshape(1, SIZE_OF_HIDDEN_LAYER_2)
        output_h2 = np.dot(output_h1, weights_h1h2) + bias_h2
        output_h2 = NeuralNetwork.relu(output_h2)

        weights_h2o = self._chromosome.weights_h2o.reshape(SIZE_OF_HIDDEN_LAYER_2, SIZE_OF_OUTPUT_NODES)
        bias_o = self._chromosome.bias_o.reshape(1, SIZE_OF_OUTPUT_NODES)
        output = np.dot(output_h2, weights_h2o) + bias_o

        return output.flatten().argmax()


