import math

import numpy as np


def weighted_choice(seq, weights):
    assert len(weights) == len(seq)
    assert abs(1. - sum(weights)) < 1e-6

    x = np.random.rand()
    for i, element in enumerate(seq):
        if x <= weights[i]:
            return i, element
        x -= weights[i]

