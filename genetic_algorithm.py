import numpy as np
import json
from chromosome import Chromosome
from constants import *
from util import weighted_choice


class GeneticAlgorithm:
    def __init__(self, from_dict={}):
        self.population = from_dict.get("population", [(lambda: Chromosome())() for _ in range(SIZE_OF_POPULATION)])
        self.episode = from_dict.get("episode", 0)
        self.stats = from_dict.get("stats", [])

    @staticmethod
    def from_json(data):
        population = []
        for c_dict in data["population"]:
            c_dict = json.loads(c_dict)
            population.append(Chromosome.from_json(c_dict))
        return GeneticAlgorithm({
            "population": population,
            "episode": data["episode"],
            "stats": data.get("stats", [])  # necessary to support old data json format
        })

    @staticmethod
    def from_file(fname='data.json'):
        with open(fname) as f:
            data = json.load(f)
            f.close()
            raw_json = json.loads(data)
            return GeneticAlgorithm.from_json(raw_json)

    def save(self):
        with open('data.json', 'w+') as f:
            json.dump(self.to_json(), f)
            f.close()

    def run(self, nep=100):
        try:
            while self.episode < nep:
                print("Episode ", self.episode, "------")

                self.evaluate_population_fitness()
                self.update_stats()

                self.generate_children()

                self.save()
                self.episode += 1

        except KeyboardInterrupt:
            print('Interrupted at episode %d' % self.episode)
            self.save()

    def pop_fitness_average(self):
        total = 0
        for chromosome in self.population:
            total += chromosome.fitness
        return total/len(self.population)

    def update_stats(self):
        fittest = self.population[0].fitness
        pop_fitness_average = self.pop_fitness_average()
        print('FITTEST', fittest)
        print('AVERAGE POP', pop_fitness_average)
        self.stats.append({
            "episode": self.episode,
            "fittest": fittest,
            "pop_fitness_average": pop_fitness_average
        })

    @staticmethod
    def probability_table(population):
        probability_table = np.array(list(map(lambda chromosome: chromosome.fitness, population)))
        probability_table = probability_table / probability_table.sum()
        return probability_table

    def evaluate_population_fitness(self):
        for chromosome in self.population:
            chromosome.evaluate()
        self.sort_population()

    def pool_selection(self):
        population = self.population.copy()
        best_parents = population[:SIZE_OF_BEST_PARENTS]
        probability_table = GeneticAlgorithm.probability_table(best_parents)

        idx, parent1 = weighted_choice(best_parents, probability_table)
        del population[idx]

        probability_table = GeneticAlgorithm.probability_table(population)
        idx2, parent2 = weighted_choice(population, probability_table)

        return parent1, parent2

    def generate_children(self):
        parent1, parent2 = self.pool_selection()
        self.exploitation_children(self.population[0])
        self.exploration_children(parent1, parent2)

    def exploitation_children(self, parent):
        children = []
        for i in range(EXPLOITATION_CHILDREN):
            child = Chromosome(parent.genes.copy())
            child.mutate()
            children.append(child)
        self.population[SIZE_OF_BEST_PARENTS:SIZE_OF_BEST_PARENTS+EXPLOITATION_CHILDREN] = children

    def exploration_children(self, parent1, parent2):
        children = []
        for i in range(EXPLORATION_CHILDREN//2):
            offsprings = parent1.crossover(parent2)
            children.append(offsprings[0])
            children.append(offsprings[1])
        self.population[SIZE_OF_BEST_PARENTS+EXPLOITATION_CHILDREN:] = children

    def sort_population(self):
        self.population.sort(key=lambda c: c.fitness, reverse=True)

    def to_json(self):
        data = { "population": [], "episode": self.episode, "stats": self.stats }
        for chromosome in self.population:
            data["population"].append(chromosome.to_json())
        return json.dumps(data)
