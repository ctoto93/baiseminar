import json

import numpy as np
import gym
from constants import *
from neural_network import NeuralNetwork

engine = gym.make('LunarLander-v2')

class Chromosome:
    def __init__(self, genes=None):
        if genes is None:
            genes = Chromosome.randomized_genes()
        self._genes = genes
        self._fitness = None

    @staticmethod
    def from_json(data):
        return Chromosome(np.array(data["genes"]))

    @staticmethod
    def randomized_genes():
        genes = []
        for index in range(SIZE_OF_GENES):
            genes.append(np.random.rand() * 2 - 1)

        return np.array(genes)

    @property
    def fitness(self):
        return self._fitness

    @property
    def genes(self):
        return self._genes

    @property
    def weights_ih1(self):
        return self._genes[:INDEX_WEIGHT_IH1]

    @property
    def weights_h1h2(self):
        return self._genes[INDEX_WEIGHT_IH1:INDEX_WEIGHT_H1H2]

    @property
    def weights_h2o(self):
        return self._genes[INDEX_WEIGHT_H1H2:INDEX_WEIGHT_H2O]

    @property
    def bias_h1(self):
        return self._genes[INDEX_WEIGHT_H2O:INDEX_BIAS_H1]

    @property
    def bias_h2(self):
        return self._genes[INDEX_BIAS_H1:INDEX_BIAS_H2]

    @property
    def bias_o(self):
        return self._genes[INDEX_BIAS_H2:]

    def crossover(self, partner):
        cut_index = np.random.randint(0, SIZE_OF_GENES)
        self_genes = self._genes.copy()
        partner_genes = partner.genes.copy()

        offspring1 = np.concatenate((self_genes[:cut_index], partner_genes[cut_index:]))
        offspring2 = np.concatenate((partner_genes[:cut_index], self_genes[cut_index:]))

        offspring1 = Chromosome(offspring1)
        offspring2 = Chromosome(offspring2)

        offspring1.mutate()
        offspring2.mutate()

        return np.array([offspring1, offspring2])

    def mutate(self):
        for index in range(SIZE_OF_GENES):
            if np.random.rand() < MUTATION_RATE:
                percent = np.random.uniform(1 - MUTATION_VARIETY, 1 + MUTATION_VARIETY)
                self._genes[index] = self._genes[index] * percent
        return

    def evaluate(self, render=False):
        brain = NeuralNetwork(self)
        fitness = 0

        for _ in range(FITNESS_TRIAL):
            if render:
                print('\nAction: ', end='')
            observation = engine.reset()

            for t in range(200):
                action = brain.predict(observation)

                if render:
                    engine.render()
                    print(action, end=' ')

                observation, reward, done, info = engine.step(action)
                fitness += max(0, reward)

                if done:
                    break

        self._fitness = fitness/FITNESS_TRIAL

    def to_json(self):
        chromosome_dict = { "genes": self.genes.tolist()}
        return json.dumps(chromosome_dict)